import { regex, list, business } from "../src";

const slugValid = "7ltGuJIIXUq2JkNV";
const slugInvalid = "7ltGuJIIXUq2JkN-";

const numberValid = "1234567890";
const numberInvalid = "%eqwe32";

const phoneNumberValid = "926348201";
const phoneNumberInvalid = "987654321a";
const phoneNumberFail = "900000000";

const merchantNameValid = "Francisco Gamonal";
const merchantNameInvalid = "Francisco Gamonal %%w343--...";

const dynamicDescriptorValid = "www.youtube.com/";
const dynamicDescriptorInvalid2 = "https://www.google.com";
const dynamicDescriptorInvalid = "Ñáño";

describe("regex", () => {
  test("is number valid", () => {
    expect(regex.onlyNumber.test(numberValid)).toBe(true);
  });

  test("is number invalid", () => {
    expect(regex.onlyNumber.test(numberInvalid)).toBe(false);
  });

  test("is merchantName valid", () => {
    expect(regex.merchantName.test(merchantNameValid)).toBe(true);
  });

  test("is merchantName invalid", () => {
    expect(regex.merchantName.test(merchantNameInvalid)).toBe(false);
  });

  test("is dynamicDescriptor valid", () => {
    expect(regex.dynamicDescriptor.test(dynamicDescriptorValid)).toBe(true);
  });

  test("is dynamicDescriptor Invalid 2", () => {
    expect(regex.dynamicDescriptor.test(dynamicDescriptorInvalid2)).toBe(false);
  });

  test("is dynamicDescriptor Invalid", () => {
    expect(regex.dynamicDescriptor.test(dynamicDescriptorInvalid)).toBe(false);
  });

  test("is slug valid", () => {
    expect(regex.slug.test(slugValid)).toBe(true);
  });

  test("is slug invalid", () => {
    expect(regex.slug.test(slugInvalid)).toBe(false);
  });
});

describe("array", () => {
  test("is phone Number valid", () => {
    expect(list.phoneFails.includes(phoneNumberValid)).toBe(false);
  });

  test("is phone Number invalid", () => {
    expect(list.phoneFails.includes(phoneNumberInvalid)).toBe(false);
  });

  test("is phone Number fake", () => {
    expect(list.phoneFails.includes(phoneNumberFail)).toBe(true);
  });
});

describe("functions", () => {
  test("when received values corrects", () => {
    expect(business.functions.formatTimeZone("America/Lima", "2019-11-21T16:29:49.516Z")).toBe("2019-11-21T11:29:49-05:00");
  });
});
