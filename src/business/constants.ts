export const nanoid = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

export const formatDate = "YYYY-MM-DDTHH:mm:ss.SSSZZZ";

export const typeUser: any = {
  developer: "Desarrollador",
  merchant: "Comercio",
};

export const states: any = {
  es: {
    registered: "registrado",
    active: "activo",
    locked: "bloqueado",
    expired: "expirado",
    inactive: "inactivo",
    pending: "pendiente",
    activating: "en activacion",
    settingUp: "en setup",
    observed: "observado",
    deleted: "eliminado",
    rejected: "rechazado",
    error: "error",
    executed: "ejecutado",
    success: "exitoso",
    approved: "aprobado",
  },
  en: {
    registered: "registered",
  },
};

export const statesPay: any = {
  es: {
    pending: "pendiente",
    rejected: "rechazado",
    expired: "expirado",
    paid: "pagado",
  },
  en: {
    pending: "pending",
  },
};

export const results: any = {
  es: {
    valid: "Válido",
    invalid: "Inválido",
  },
};

export const accountValidationsResults: any = {
  es: {
    valid: "Válida",
    invalid: "Inválida",
    approved: "Aprobada",
    observed: "Observada",
    rejected: "Rechazada",
  },
};

export const accountValidationsStatus: any = {
  es: {
    pending: "Pendiente",
    error: "Error",
    executed: "Ejecutada",
  },
};

export const statusTransactionMpos: any = {
  es: {
    authorized: "Autorizada",
    voided: "Anulada",
    denied: "Denegada",
    reversed: "Extornada",
  },
};

export const eventTransactionMpos: any = {
  es: {
    authorization: "Autorización",
    refund: "Devolución",
  },
};

export const statusTransaction: any = {
  es: {
    registered: "Registrada",
    authorized: "Autorizada",
    voided: "Anulada",
    denied: "Denegada",
    reversed: "Extornada",
    captured: "Capturada",
    paid: "Pagada",
    held: "Retenida",
    deposited: "Depositado",
    refunded: "reembolsado",
    fraudulent: "fraudulenta",
  },
};

export const eventTransaction: any = {
  es: {
    register: "Registrarse",
    authorize: "Autorizar",
    deny: "Denegar",
    capture: "Capturar",
    totalCancellation: "Cancelación total",
    pay: "Pagar",
    hold: "Retener",
    refund: "Reembolso",
    fraud: "Fraude",
    deposit: "Depositar",
    partialCancellation: "Cancelación parcial",
  },
};

export const statusPayment: any = {
  es: {
    registered: "Registrada",
    denied: "Denegada",
    pending: "Pendiente",
    expired: "Expirada",
    deleted: "Eliminada",
    successful: "Existosa",
    paid: "Pagada",
    held: "Retenida",
    deposited: "Depositado",
  },
};

export const eventPayment: any = {
  es: {
    register: "Registrarse",
    deny: "Denegar",
    create: "Crear",
    expire: "Expirar",
    delete: "Eliminar",
    success: "Exito",
    pay: "Pagar",
    hold: "Retener",
    deposit: "Sepositar",
  },
};

export const typeTransactions: any = {
  charge: "Cargo",
  payment: "Orden",
};
