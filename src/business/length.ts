export const ruc = 11;

export const dni = 8;

export const ce = 9;

export const nameMin = 2;
export const nameMax = 100;

export const addressMin = 10;
export const addressMax = 150;

export const emailMax = 75;

export const bcp: any = {
  ahorro: 14,
  default: 13,
};

export const bbva: any = {
  default: 18,
};

export const interbank: any = {
  default: 13,
};

export const scotiabank: any = {
  ahorro: 10,
  default: 14,
};

export const nanoid = 16;
