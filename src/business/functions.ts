import moment from "moment-timezone";

export const nFilter = (filter: object) => {
  const obj: any = {};
  Object.entries(filter).forEach(([k, v]) => {
    if (k.indexOf(".$l") !== -1) {
      obj[k.substring(0, k.indexOf(".$l"))] = new RegExp(`${v}`, "i");
    } else if (k.indexOf(".$in") !== -1 && Array.isArray(v)) {
      obj[k.substring(0, k.indexOf(".$in"))] = v;
    } else if (k.indexOf(".$nin") !== -1 && Array.isArray(v)) {
      obj[k.substring(0, k.indexOf(".$nin"))] = v;
    } else if (k.indexOf(".$rd") !== -1) {
      const f: any = {};
      v[">="] && (f.$gte = moment(v[">="]).startOf("day").toDate());
      v["<="] && (f.$lte = moment(v["<="]).endOf("day").toDate());
      v[">"] && (f.$gt = moment(v[">"]).startOf("day").toDate());
      v["<"] && (f.$lt = moment(v["<"]).endOf("day").toDate());
      obj[k.substring(0, k.indexOf(".$rd"))] = f;
    } else if (k.indexOf(".$fd") !== -1) {
      const f: any = {};
      const dbGte = moment(v[">="]).utc().toDate();
      const dbLte = moment(v["<="]).utc().toDate();
      const dbGt = moment(v[">"]).utc().toDate();
      const dbLt = moment(v["<"]).utc().toDate();
      v[">="] && (f.$gte = dbGte);
      v["<="] && (f.$lte = dbLte);
      v[">"] && (f.$gt = dbGt);
      v["<"] && (f.$lt = dbLt);
      obj[k.substring(0, k.indexOf(".$fd"))] = f;
    } else if (k === "$or" && Array.isArray(v)) {
      obj[k] = v.map((e) => nFilter(e));
    } else {
      obj[k] = v;
    }
  });
  return obj;
};

// eslint-disable-next-line @typescript-eslint/no-inferrable-types
export const filterForQuery: any = async (objectOptions: any) => {
  const { numPage, numLimit, model, fields, lean, orderBy } = objectOptions;
  let { filter } = objectOptions;
  const objFields: any = { _id: 0 };
  let order: any = { createdAt: -1 };
  filter && (filter = nFilter(filter));
  if (fields) {
    objFields["slug"] = 1;
    fields.forEach(function (data: any) {
      objFields[data] = 1;
    });
  }
  if (orderBy) {
    order = orderBy;
  }
  let items = [];
  if (lean) {
    items = await model
      .find(filter, objFields)
      .sort(order)
      .skip((numPage - 1) * numLimit)
      .limit(numLimit)
      .lean();
  } else {
    items = await model
      .find(filter, objFields)
      .sort(order)
      .skip((numPage - 1) * numLimit)
      .limit(numLimit)
      .exec();
  }
  const total = await model.find(filter).countDocuments();
  const totalPage = total / numLimit;
  const totalPageCeil = Math.ceil(totalPage);

  return {
    items,
    total,
    page: numPage,
    limit: numLimit,
    totalPage: totalPageCeil,
  };
};

export const onPagination: any = async (objectOptions: any) => {
  const { numPage, numLimit, model, fields, orderBy } = objectOptions;
  const objFields: any = { _id: 0 };
  let order: any = { createdAt: -1 };
  if (fields) {
    objFields["slug"] = 1;
    fields.forEach(function (data: any) {
      objFields[data] = 1;
    });
  }
  if (orderBy) {
    order = orderBy;
  }

  const item = await model
    .find({}, objFields)
    .sort(order)
    .skip((numPage - 1) * numLimit)
    .limit(numLimit)
    .exec();
  const total = await model.countDocuments();
  const totalPage = total / numLimit;
  const totalPageCeil = Math.ceil(totalPage);
  return {
    items: item,
    total,
    page: numPage,
    limit: numLimit,
    totalPage: totalPageCeil,
  };
};

export const formatTimeZone: any = (timeZone: string, time: string) => {
  const timeInitial = moment(time);
  const timeZoneWanted = timeInitial.clone().tz(timeZone);
  return timeZoneWanted.format();
};
