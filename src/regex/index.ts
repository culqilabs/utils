export const lowerCase = new RegExp("^(?=.*[a-z])");
export const upperCase = new RegExp("^(?=.*[A-Z])");

export const onlyNumber = new RegExp("^[0-9]*$");

export const phoneNumber = new RegExp("^((9)[0-9]{8})$");

export const merchantName = new RegExp("^[A-Za-záéíóúñÁÉÍÓÚÑ0-9\\s-.,/]+$");

export const name = new RegExp("^([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\\-\\']+[\\s])+([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\\-\\'])+[\\s]?([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\\-\\'])?$");

export const address = new RegExp("^[A-Za-záéíóúñÁÉÍÓÚÑ0-9\\s\\-\\.\\,\\/]+$");

export const email = new RegExp("^[a-zA-Z0-9][\\w\\.-]+[a-zA-Z0-9]@[\\w-\\.]*[a-zA-Z0-9]\\.[a-zA-Z]{2,7}");

export const notSpace = new RegExp("^\\s");

export const url = new RegExp("^(http://www.|https://www.|http://|https://)?[a-z0-9]+([-.]{1}[a-z0-9]+)*.[a-z]{2,5}(:[0-9]{1,5})?(/.*)?");

export const mongoId = new RegExp("^[0-9a-fA-F]{24}$");

export const passwordLength = new RegExp("^.{8,16}$");
export const passwordSpecialCharacter = new RegExp("[!@#$%^&*(),.?\":{}|<>'~`_;\\[\\]\\/-]");

export const slug = new RegExp("^[a-zA-Z0-9]{16}$");

export const binNumber = new RegExp("^[0-9]{6,8}$");

export const dynamicDescriptor = new RegExp(/^([A-Za-z0-9\\s\-.,' ]+(\/?))$/);
